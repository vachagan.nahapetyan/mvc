<?php
//
//class Router
//{
//    // Будут хранится маршруты в виде массива
//    private $routes;
//
//    /**
//     * Получаем из файла routes.php массив с роутами
//     * и присваиваем закрытому свойству $routes
//     *
//     * Router constructor.
//     */
//    public function __construct()
//    {
//        // Путь к базовой директории и путь к роутам
//        $routesPath = ROOT . '/config/routes.php';
//        // После require_once в $this->routes хранится массив с роутами
//        $this->routes = require_once $routesPath;
////        echo '<pre>';
////        print_r($this->routes);
//    }
//
//    /**
//     * Returns request string ( Возвращает строку запроса )
//     *
//     * @return string
//     */
//    private function getURI()
//    {
//        if (!empty($_SERVER['REQUEST_URI'])) {
//            return trim($_SERVER['REQUEST_URI'], '/');
//        }
//    }
//
//    public function run()
//    {
//        // 1. Получаем строку запроса
//        $uri = $this->getURI();
////         echo $uri;
////         news/5
//
//        // 2. Проверяем наличие такого запроса в файле routes.php
////        echo '<pre>';
////        echo $uri;
////        echo '<br>';
////        print_r($this->routes);
////        echo '</pre>'; die;
//
//        foreach ($this->routes as $uriPattern => $path) {
//            // Сравниваем $uriPattern и $uri и если есть совпадение то в текущем
//            // $path будет хранится строка с именем controller-а и action-а
////             echo "~$uriPattern~"."<br>";
//            if (preg_match("~\b$uriPattern\b~", $uri)) {
////                echo '<br> Где ищем (запрос котоорый набрал пользоваель): ' . $uri;
////                echo '<br> Что ищем (совпадение из правила): ' . $uriPattern;
////                echo '<br> Кто обрабатывает: ' . $path;
//                $internalRoute = preg_replace("~$uriPattern~", $path, $uri);
////                $internalRoute = preg_replace("~news/([0-9]+)~", "news/view/$1", 'news/12');
////                echo  ' шаблон-  '.$uriPattern."<br>";
////                echo 'заменяет их на-  '. $path."<br>";
////                echo  'совпадений в строке-  '.$uri."<br>";
////                echo "результат-  ".$internalRoute;
//
////                 echo '<br> Нужно сформировать: ' . $internalRoute . '<br>';
////                 news/view/5
//
//                // 3. Если есть совпадение, определяем какой controller и action обрабатывают запрос
//
//                // Разбиваем строку на 2 части чтобы отдельно получить
//                // имя controller-а и action-а
//                $segments = explode('/', $internalRoute);
////                echo '<pre>';
////                print_r($segments);
//
//
//                // Собираем по кусочкам имя controller-а
//                $controllerName = ucfirst(array_shift($segments)) . 'Controller';
////                echo $controllerName;
////                echo '<pre>';
////                print_r($segments);
//
//                // Собираем по кусочкам имя action-а
//                $actionName = "action" . ucfirst(array_shift($segments));
////                echo '<br>' . $actionName;
////                echo '<pre>';
////                print_r($segments);
//
//                $parameters = $segments;
////                echo '<pre>';
////                print_r($parameters);
//
//                // 4. Подключаем файл класса-контроллера
//                $controllerFile = ROOT . '/controllers/' . $controllerName . '.php';
//                if (file_exists($controllerFile)) {
//                    require_once($controllerFile);
//                }
//
//                // 5. Создаем объект и вызываем нужный метод (т.е. action)
//                $controllerObject = new $controllerName;
//                $result = call_user_func_array([$controllerObject, $actionName], $parameters);
//                if ($result) {
//                    break;
//                }
//            }
//        }
//    }
//}


class Router
{
    private $routes;

    public function __construct()
    {
        $routepath = ROOT . '/config/routes.php';

        $this->routes = require_once $routepath;

    }

    private function getURI()
    {

        if (!empty($_SERVER['REQUEST_URI'])) {

            return trim($_SERVER['REQUEST_URI'], '/');

        }
    }

    public function run()
    {

        $uri = $this->getURI();

        foreach ($this->routes as $routePath => $path) {

            if (preg_match("~\b$routePath\b~", $uri)) {

                $internalRoute = preg_replace("~\b$routePath\b~", $path, $uri);

                $segments = explode('/', $internalRoute);

                $controllerName = ucfirst(array_shift($segments)) . 'Controller';

                $actionName = 'action' . ucfirst(array_shift($segments));

                $parameters = $segments;

                $controllerFile = ROOT . '/controllers/' . $controllerName . '.php';

                if (file_exists($controllerFile)) {

                    require_once($controllerFile);

                }


                $objectName = new $controllerName;

                $result = call_user_func_array([$objectName, $actionName], $parameters);

                if ($result) {

                    break;

                }
            }
        }
    }
}

