<?php

/*
 * Возвращаем ассоциативный массив где в качастве ключей выступает строка запроса
 * а значения - то где обрабатывается запрос
 */
return [
//    '^news\/([a-z]+)\/([0-9]+)$' => 'news/$1/$2',
    'news\/([0-9]+)$' => 'news/view/$1',
    '^news$' => 'news/index',


//    'news' => 'news/index', // actionIndex в NewsController
//    'products' => 'product/list', // actionList в ProductController
];